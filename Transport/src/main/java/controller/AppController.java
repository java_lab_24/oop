package controller;

import model.TrainService;
import model.domain.train.Train;
import model.domain.carriage.Carriage;
import view.ConsoleView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class AppController {

  private LinkedHashMap<String, String> mainMenu;
  private ArrayList<LinkedHashMap<String, String>> subMenus;
  private String userMenuInput;
  private static LinkedList<Train> trains;
  private static final int NO_DATA = 0;

  public AppController() {
    mainMenu = new LinkedHashMap<>();
    setMainMenu();
    subMenus = new ArrayList<>();
    setFirstSubMenu();
    setSecondSubMenu();
    setThirdSubMenu();
    userMenuInput = new String();
    trains = new LinkedList<>();
  }

  private void setMainMenu() {
    mainMenu.put("1", "Get all available trains");
    mainMenu.put("2", "Create new train");
    mainMenu.put("q", "Exit");
  }

  private void setFirstSubMenu() {
    LinkedHashMap<String, String> subMenu = new LinkedHashMap<String, String>();
    subMenu.put("1", "Get more information about specific train");
    subMenu.put("q", "Go back to main menu");
    subMenus.add(0, subMenu);
  }

  private void setSecondSubMenu() {
    LinkedHashMap<String, String> subMenu = new LinkedHashMap<String, String>();
    subMenu.put("1", "Get locomotive information");
    subMenu.put("2", "Get carriage information");
    subMenu.put("q", "Go back to main menu");
    subMenus.add(1, subMenu);
  }

  private void setThirdSubMenu() {
    LinkedHashMap<String, String> subMenu = new LinkedHashMap<String, String>();
    subMenu.put("1", "Add new locomotive");
    subMenu.put("2", "Add new carriage");
    subMenu.put("q", "Go back to main menu");
    subMenus.add(2, subMenu);
  }

  public void start() {
    while (!userMenuInput.equals("q")) {
      ConsoleView.print(mainMenu);
      userMenuInput = ConsoleView.getUserMenuInput();
      switch (userMenuInput) {
        case "1":
          if (trains.size() != NO_DATA) {
            ConsoleView.print(trains);
            ConsoleView.print(subMenus.get(0));
            userMenuInput = ConsoleView.getUserMenuInput();
            switch (userMenuInput) {
              case "1":
                ConsoleView.printRequest("Enter train name: ");
                userMenuInput = ConsoleView.getUserInput();
                Train tmpTrain = TrainService.findTrainByName(trains, userMenuInput);
                if (tmpTrain != null) {
                  ConsoleView.printMessage(TrainService.getTrainDetail(tmpTrain));
                } else {
                  ConsoleView.printMessage(ConsoleView.NO_DATA_FOUND);
                }
                break;
              case "q":
                userMenuInput = "go back to main";
                break;
              default:
                System.out.println("\nBad input. Try again!");
                break;
            }
            break;
          } else {
            ConsoleView.printMessage(ConsoleView.NO_DATA_FOUND);
          }
          break;
        case "2":
          TrainService.createNewTrainAndToList(trains);
          LinkedList<Carriage> carriages = new LinkedList<Carriage>();
          while (!userMenuInput.equals("go back to main")) {
            ConsoleView.print(subMenus.get(2));
            userMenuInput = ConsoleView.getUserMenuInput();
            switch (userMenuInput) {
              case "1":
                trains.getLast().setLocomotive(TrainService.createNewLocomotive());
                break;
              case "2":
                carriages.add(TrainService.createNewCarriage());
                break;
              case "q":
                trains.getLast().setCarriages(carriages);
                userMenuInput = "go back to main";
                break;
              default:
                System.out.println("\nBad input. Try again!");
                break;
            }
          }
          break;
        case "q":
          System.out.println("\nExiting...");
          break;
        default:
          System.out.println("\nBad input. Try again!");
          break;
      }
    }
  }
}
