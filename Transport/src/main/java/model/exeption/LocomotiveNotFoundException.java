package model.exeption;

public class LocomotiveNotFoundException extends TrainException {

  public LocomotiveNotFoundException() {
  }

  public LocomotiveNotFoundException(String message) {
    super(message);
  }

  public LocomotiveNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public LocomotiveNotFoundException(Throwable cause) {
    super(cause);
  }
}
