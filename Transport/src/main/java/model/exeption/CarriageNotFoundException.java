package model.exeption;

public class CarriageNotFoundException extends TrainException {

  public CarriageNotFoundException() {
  }

  public CarriageNotFoundException(String message) {
    super(message);
  }

  public CarriageNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public CarriageNotFoundException(Throwable cause) {
    super(cause);
  }
}
