package model.domain.locomotive;

import model.domain.carriage.Carriage;

import java.util.List;

public class Locomotive implements LocomotiveCalculator {

  private String name;
  private float maxSpeed;
  private float efficiency;
  private float weigh;
  private LocomotiveEngineType locomotiveEngineType;

  public String getName() {
    return name;
  }

  public float getMaxSpeed() {
    return maxSpeed;
  }

  public float getEfficiency() {
    return efficiency;
  }

  public float getWeigh() {
    return weigh;
  }

  public LocomotiveEngineType getLocomotiveEngineType() {
    return locomotiveEngineType;
  }

  private Locomotive(LocomotiveBuilder locomotiveBuilder) {
    this.name = locomotiveBuilder.name;
    this.maxSpeed = locomotiveBuilder.maxSpeed;
    this.efficiency = locomotiveBuilder.efficiency;
    this.weigh = locomotiveBuilder.weigh;
    this.locomotiveEngineType = locomotiveBuilder.locomotiveEngineType;
  }

  public boolean canCarry(List<Carriage> carriages) {
    return false;
  }

  public float getMaxSpeedWithLoad(List<Carriage> carriages) {
    return 0;
  }

  public static class LocomotiveBuilder {

    private String name;
    private LocomotiveEngineType locomotiveEngineType;
    private float maxSpeed;
    private float weigh;
    private float efficiency;

    public LocomotiveBuilder setName(String name) {
      this.name = name;
      return this;
    }

    public LocomotiveBuilder setLocomotiveEngineType(String locomotiveEngineType) {
      this.locomotiveEngineType = LocomotiveEngineType.
          valueOf(locomotiveEngineType.toUpperCase().trim());
      return this;
    }

    public LocomotiveBuilder setMaxSpeed(float maxSpeed) {
      this.maxSpeed = maxSpeed;
      return this;
    }

    public LocomotiveBuilder setWeigh(float weigh) {
      this.weigh = weigh;
      return this;
    }

    public LocomotiveBuilder setEfficiency(float efficiency) {
      this.efficiency = efficiency;
      return this;
    }

    public Locomotive build() {
      return new Locomotive(this);
    }
  }
}
