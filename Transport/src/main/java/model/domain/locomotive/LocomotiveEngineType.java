package model.domain.locomotive;

public enum LocomotiveEngineType {
  STEAM, ELECTRIC, THERMAL, ELECTRICAL_THERMAL, ELECTRIC_BATTERY;

  @Override
  public String toString() {
    return this.name();
  }
}
