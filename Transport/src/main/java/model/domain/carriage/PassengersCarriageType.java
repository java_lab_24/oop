package model.domain.carriage;

public enum PassengersCarriageType {
  COMPARTMENT, ECONOMY_CLASS, SITTING_FIRST_CLASS, SITTING_SECOND_CLASS;

  @Override
  public String toString() {
    return this.name();
  }
}
