package model.domain.carriage;

public enum FreightCarriageType {
  COVERED, TANK, PLATFORM, FITTING_PLATFORM, HOPPER;

  @Override
  public String toString() {
    return this.name();
  }
}
