package model;

import model.domain.carriage.*;
import model.domain.locomotive.LocomotiveEngineType;
import model.domain.train.Train;
import model.exeption.CarriageNotFoundException;
import model.exeption.LocomotiveNotFoundException;
import model.domain.locomotive.Locomotive;
import view.ConsoleView;

import java.util.LinkedList;
import java.util.Scanner;

public class TrainService {

  public static Train findTrainByName(LinkedList<Train> trains, String trainName) {
    for (Train t : trains) {
      if (t.getName().equals(trainName)) {
        return t;
      }
    }
    return null;
  }

  public static String getTrainDetail(Train train) {
    String trainDetail = new String();
    try {
      if (train.getLocomotive() == null) {
        throw new LocomotiveNotFoundException("\nLocomotive not found. Please add it first");
      } else if (train.getCarriages() == null) {
        throw new CarriageNotFoundException("\nCarriage not found. Please add them first");
      }
      trainDetail = train.getName() + "\n" +
          "/|" + train.getLocomotive().getName() + "|";
      for (Carriage c : train.getCarriages()) {
        trainDetail += "._.|";
        trainDetail +=
            " " + ((c instanceof PassengersCarriage) ? "PassengersCarriage" : "FreightCarriage");
        trainDetail += "|";
      }
    } catch (LocomotiveNotFoundException e) {
      System.err.println(e);
    } catch (CarriageNotFoundException e) {
      System.err.println(e);
    }
    return trainDetail;
  }

  public static void createNewTrainAndToList(LinkedList<Train> trains) {
    ConsoleView.printRequest("Enter train name");
    String trainName = ConsoleView.getUserInput();
    ConsoleView.printRequest("Enter goods weigh");
    float goodsWeigh = Float.valueOf(ConsoleView.getUserInput());
    ConsoleView.printRequest("Enter passengers number");
    int passengersNumber = Integer.valueOf(ConsoleView.getUserInput());
    Train.TrainBuilder tb = new Train.TrainBuilder().setName(trainName).setGoodsWeigh(goodsWeigh)
        .setPassengersNumber(passengersNumber);
    trains.add(tb.build());
    ConsoleView.printMessage("Train " + trains.getLast().getName() + " created");
  }

  public static Locomotive createNewLocomotive() {
    Scanner scanner = new Scanner(System.in);
    ConsoleView.printRequest("Enter locomotive name");
    String locomotiveName = ConsoleView.getUserInput();
    ConsoleView.printRequest("Enter max speed");
    float maxSpeed = Float.valueOf(ConsoleView.getUserInput());
    ConsoleView.printRequest("Enter efficiency: ");
    float efficiency = Float.valueOf(ConsoleView.getUserInput());
    ConsoleView.printRequest("Enter weigh: ");
    float weigh = Float.valueOf(ConsoleView.getUserInput());
    ConsoleView.printMessage("Engine types (" + getLocomotiveEngineTypes() + ")");
    ConsoleView.printRequest("Enter engine type: ");
    String engineType = ConsoleView.getUserInput();
    Locomotive.LocomotiveBuilder lb = new Locomotive.LocomotiveBuilder().setName(locomotiveName).
        setMaxSpeed(maxSpeed).setEfficiency(efficiency).setWeigh(weigh).
        setLocomotiveEngineType(engineType);
    return lb.build();
  }

  public static Carriage createNewCarriage() {
    Scanner scanner = new Scanner(System.in);
    ConsoleView.printRequest("Enter carrying capacity");
    int carryingCapacity = Integer.valueOf(ConsoleView.getUserInput());
    ConsoleView.printRequest("Enter weigh of carriage");
    int weigh = Integer.valueOf(ConsoleView.getUserInput());
    ConsoleView.printRequest("Would it be passengers carriage?(y/n)");
    String isPassengerType = ConsoleView.getUserInput();
    Carriage carriage;
    if (isPassengerType.equals("y")) {
      ConsoleView.printMessage("Passenger carriage types (" + getPassengerCarriageTypes() + ")");
      ConsoleView.printRequest("Enter passengers carriage type");
      String passengersCarriageType = ConsoleView.getUserInput();
      carriage = new PassengersCarriage(carryingCapacity, weigh, passengersCarriageType);
    } else {
      ConsoleView.printMessage("Freight carriage types (" + getFreightCarriageTypes() + ")");
      ConsoleView.printRequest("Enter freight carriage type");
      String freightCarriageType = ConsoleView.getUserInput();
      carriage = new FreightCarriage(carryingCapacity, weigh, freightCarriageType);
    }
    return carriage;
  }

  private static String getLocomotiveEngineTypes() {
    LocomotiveEngineType[] locomotiveEngineTypes = LocomotiveEngineType.values();
    String values = new String();
    for (LocomotiveEngineType l : locomotiveEngineTypes) {
      values += l.toString() + ", ";
    }
    return values;
  }

  private static String getPassengerCarriageTypes() {
    PassengersCarriageType[] passengersCarriageTypes = PassengersCarriageType.values();
    String values = new String();
    for (PassengersCarriageType l : passengersCarriageTypes) {
      values += l.toString() + ", ";
    }
    return values;
  }

  private static String getFreightCarriageTypes() {
    FreightCarriageType[] freightCarriageTypes = FreightCarriageType.values();
    String values = new String();
    for (FreightCarriageType l : freightCarriageTypes) {
      values += l.toString() + ", ";
    }
    return values;
  }

}
